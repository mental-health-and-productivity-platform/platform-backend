FROM python:3.9-slim

ADD . /app
WORKDIR /app

RUN apt-get update && apt-get install -y build-essential && apt-get install -y libpq-dev
RUN pip install --no-cache-dir -r requirements.txt
RUN chmod +x /app/entrypoint.sh
EXPOSE 8000

ENTRYPOINT ["/app/entrypoint.sh"]