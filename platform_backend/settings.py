from .settings_prod import *

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "mydatabase",
    }
}

MIDDLEWARE = ["corsheaders.middleware.CorsMiddleware"] + MIDDLEWARE
CORS_ORIGIN_WHITELIST = [
    "http://localhost:8080",
]
CORS_ALLOW_CREDENTIALS = True
