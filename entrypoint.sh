#!/usr/bin/env bash

python manage.py migrate
export DJANGO_SETTINGS_MODULE=platform_backend.settings_prod
gunicorn platform_backend.wsgi:application --bind 0.0.0.0:8000
