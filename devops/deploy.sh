#!/usr/bin/env bash
ssh -o StrictHostKeyChecking=no -i "Ayvar-Key-Franfurt.pem" ec2-user@ec2-18-195-96-133.eu-central-1.compute.amazonaws.com << 'ENDSSH'
 ls | cat
 wget -qO- eth0.me | cat
 cd platform-backend
 docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
 docker pull index.docker.io/nnvnick/platform_backend:latest
 docker kill backend
 docker rm backend
 sudo docker run -d -p 8000:8000 --name backend --ip 1.2.3.4 --net mynet -e DJANGO_SETTINGS_MODULE=platform_backend.settings_prod index.docker.io/nnvnick/platform_backend:latest
ENDSSH
