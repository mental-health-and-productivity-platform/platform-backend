from django.urls import re_path, include, path

from .views import RegistrationAPIView, change_user_settings
from .views import LoginAPIView
from .views import change_user_settings

urlpatterns = [
    re_path(r'^registration/?$', RegistrationAPIView.as_view(), name='user_registration'),
    re_path(r'^login/?$', LoginAPIView.as_view(), name='user_login'),
    path('user_settings/', change_user_settings),
]