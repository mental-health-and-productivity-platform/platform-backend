from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny

from .models import User
from .serializers import LoginSerializer
from .serializers import RegistrationSerializer
from .serializers import UserSerializer
from .services import JWTPermission

import jwt
from platform_backend import settings

class RegistrationAPIView(APIView):
    """
    Registers a new user.
    """
    permission_classes = [AllowAny]
    serializer_class = RegistrationSerializer

    def post(self, request):
        """
        Creates a new User object.
        Username, email, and password are required.
        Returns a JSON web token.
        """
        print("reg_post")
        print(request.data)
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {
                'token': serializer.data.get('token', None),
            },
            status=status.HTTP_201_CREATED,
        )


class LoginAPIView(APIView):
    """
    Logs in an existing user.
    """
    permission_classes = [AllowAny]
    serializer_class = LoginSerializer

    def post(self, request):
        """
        Checks is user exists.
        Email and password are required.
        Returns a JSON web token.
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = Response(serializer.data, status=status.HTTP_200_OK)
        #import jwt
        #from django.conf import settings
        #print(jwt.decode(serializer.data['token'], settings.SECRET_KEY, algorithms=["HS256"]))

        return response

    def get(self, request):
        print(request.COOKIES)
        return Response('kek', status=status.HTTP_200_OK)

@api_view(['POST'])
@permission_classes([JWTPermission])
def change_user_settings(request):
    token = request.COOKIES.get('auth_token')
    payload = jwt.decode(token, settings.SECRET_KEY, algorithms='HS256')
    user_id = payload['id']
    user_data = UserSerializer(data=request.data)
    if user_data.is_valid():
        user_instance = User.objects.get(pk=user_id)
        user_instance.email = user_data.data.get('email', user_instance.email)
        user_instance.username = user_data.data.get('username', user_instance.username)
        if 'password' in user_data.data:
            if 'old_password' not in user_data.data or (not user_instance.check_password(user_data.data['old_password'])):
                return Response('You should enter correct old password to change the password!', status=status.HTTP_400_BAD_REQUEST)
            user_instance.set_password(user_data.data['password'])
        try:
            user_instance.save()
        except Exception as err:
            return Response(str(err), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response('OK', status=status.HTTP_200_OK)
    return Response('Invalid data format', status=status.HTTP_500_INTERNAL_SERVER_ERROR)
