# Generated by Django 4.0.1 on 2022-06-09 16:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('selfbuild', '0002_remove_exercise_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='completed_exercise',
            name='time',
            field=models.DateTimeField(blank=True, null=True, verbose_name='time'),
        ),
    ]
