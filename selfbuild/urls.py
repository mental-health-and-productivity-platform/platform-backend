from django.urls import path
from . import views

urlpatterns = [
    path("", views.CourseView.as_view({"get": "get"})),
    path("<int:course_id>/", views.CourseView.as_view({"get": "get_by_id"})),
    path("<int:course_id>/blocks/<int:day_number>", views.get_block),
    path("exercise_action/<int:exercise_id>", views.change_exercise_completion),
    path("my_courses/", views.get_user_courses),
    path("<int:course_id>/pay/create/", views.create_payment),
    path("<int:course_id>/pay/confirmation/", views.confirm_payment),
    path("create_course/", views.create_course),
    path("create_block/", views.create_block),
    path("create_exercise/", views.create_exercise),
    path("test/", views.save_image),
]
