from rest_framework.serializers import ModelSerializer
from api_auth.serializers import UserShortSerializer
from .models import Course, Review, Author, Block, Exercise, Completed_Exercise


class ExerciseSerializer(ModelSerializer):
    class Meta:
        model = Exercise
        fields = "__all__"


class CompletedExerciseSerializer(ModelSerializer):
    class Meta:
        model = Completed_Exercise
        fields = "__all__"


class ReviewSerializer(ModelSerializer):
    class Meta:
        model = Review
        fields = "__all__"


class AuthorSerializer(ModelSerializer):
    class Meta:
        model = Author
        fields = "__all__"


class BlockSerializer(ModelSerializer):
    class Meta:
        model = Block
        fields = "__all__"


class CoursesSerializer(ModelSerializer):

    authors = AuthorSerializer(many=True, read_only=True)

    class Meta:
        model = Course
        fields = ["name", "description_short", "image", "id", "authors"]


class CourseSerializer(ModelSerializer):

    reviews = ReviewSerializer(many=True, read_only=True)
    authors = AuthorSerializer(many=True, read_only=True)
    blocks = BlockSerializer(many=True, read_only=True)
    users = UserShortSerializer(many=True, read_only=True)

    class Meta:
        model = Course
        fields = "__all__"
        depth = 1
