from django.db import models
from api_auth.models import User


class Course(models.Model):
    name = models.CharField("name", max_length=200, default="")
    users = models.ManyToManyField(
        User,
        related_name="courses",
        blank=True,
    )
    image = models.ImageField(upload_to="images/", blank=True)
    video = models.CharField("video", max_length=200, default="")
    description = models.TextField("description", blank=True)
    description_short = models.CharField(
        "description_short", max_length=1024, blank=True
    )
    price = models.IntegerField("price", default=0)


class Author(models.Model):
    first_name = models.CharField("first_name", max_length=100)
    last_name = models.CharField("last_name", max_length=100)
    description = models.CharField("description", max_length=100)
    image = models.ImageField(upload_to="author_images/", blank=True)
    course = models.ForeignKey(Course, related_name="authors", on_delete=models.CASCADE)


class Review(models.Model):
    first_name = models.CharField("first_name", max_length=100)
    last_name = models.CharField("last_name", max_length=100)
    description = models.CharField("description", max_length=300)
    image = models.ImageField(upload_to="review_images/", blank=True)
    grade = models.IntegerField("grade", default=0)
    course = models.ForeignKey(Course, related_name="reviews", on_delete=models.CASCADE)


class Block(models.Model):
    name = models.CharField("name", max_length=200, default="")
    day_number = models.IntegerField("day_number", default=0)
    description = models.TextField("description", blank=True)
    course = models.ForeignKey(
        Course, related_name="blocks", on_delete=models.CASCADE, blank=True
    )


class Exercise(models.Model):
    description = models.CharField("description", max_length=1000, default="")
    users = models.ManyToManyField(User, related_name="exercises", blank=False)
    block = models.ForeignKey(
        "block", related_name="exercises", blank=True, on_delete=models.CASCADE
    )


class Completed_Exercise(models.Model):
    exercise = models.ForeignKey(
        Exercise, related_name="completed_exercises", on_delete=models.CASCADE
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="completed_exercises")
    time = models.DateTimeField("time", blank=True, null=True)
    is_completed = models.BooleanField("is_completed", default=False)


class Payment(models.Model):
    status = models.CharField("status", max_length=30, default="pending")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="payments")
    course = models.ForeignKey(
        Course, on_delete=models.CASCADE, related_name="payments"
    )
    yokassa_payment_id = models.CharField("yokassa_payment_id", max_length=40)
    url = models.URLField("url", max_length=300, default="")
