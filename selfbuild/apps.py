from django.apps import AppConfig


class SelfbuildConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'selfbuild'
