import uuid

from yookassa import Configuration, Payment

# shopId 904273
# test_*gvtc02yYMZOm6jW1e9RRs-9bXhWNSksSh81yz3au5QWs

Configuration.account_id = 904273
Configuration.secret_key = "test_iKpBVGZF5IABRbdus-AGXEbNkGtE8cMCnrESnnnpf90"


def get_payment_for_course(course, redirect_url):
    payment = Payment.create(
        {
            "amount": {"value": course.price, "currency": "RUB"},
            "confirmation": {
                "type": "redirect",
                "return_url": redirect_url,
            },
            "capture": True,
            "description": f"Оплата курса {course.name}",
        },
        uuid.uuid4(),
    )
    return payment


def check_if_payment_succeed(payment_id):
    payment_status = Payment.find_one(payment_id).status
    if payment_status == "succeeded":
        return True
    return False


def get_payment(payment_id):
    payment = Payment.find_one(payment_id)
    return payment


def cancel_payment(payment_id):
    idempotence_key = str(uuid.uuid4())
    Payment.cancel(payment_id, idempotence_key)
