from django.contrib import admin
from .models import Course, Block, Exercise, Completed_Exercise, Author

admin.site.register(Course)
admin.site.register(Block)
admin.site.register(Exercise)
admin.site.register(Completed_Exercise)
admin.site.register(Author)