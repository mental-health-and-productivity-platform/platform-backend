from django.shortcuts import get_object_or_404
from django.db.models import Q
from django.utils import timezone

from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny

from api_auth.services import JWTPermission, get_user_from_cookie

from .models import Course, Block, Exercise, Completed_Exercise, Payment
from api_auth.models import User
from api_auth.services import JWTPermission
from . import serializers
from .services import get_payment_for_course, check_if_payment_succeed


class CourseView(ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = Course.objects.all()
    serializer_class = serializers.CoursesSerializer

    def get(self, request):
        if request.GET.get("search", "") != "":
            search_str = request.GET.get("search", "")
            courses_found = Course.objects.all()
            for word in search_str.split():
                courses_found = courses_found.filter(name__icontains=word)
            serializer = self.serializer_class(courses_found, many=True)
        else:
            serializer = self.serializer_class(Course.objects.all(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_by_id(self, request, course_id):
        course = Course.objects.get(id=course_id)
        serializer = serializers.CourseSerializer(course)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes([JWTPermission])
def get_user_courses(request):
    user = get_user_from_cookie(request)
    payments = Payment.objects.filter(user=user, status="pending")
    for payment in payments:
        if check_if_payment_succeed(payment.yokassa_payment_id):
            payment.status = "succeeded"
            for block_instance in payment.course.blocks.all():
                for exercise_instance in block_instance.exercises.all():
                    compl_exer = Completed_Exercise(exercise=exercise_instance, user=user, is_completed=False)
                    compl_exer.save()
            user.courses.add(payment.course)
            user.save()
            payment.save()
    courses = user.courses.all()
    search_str = request.GET.get("search", "")
    if search_str != "":
        for word in search_str.split():
            courses = courses.filter(name__icontains=word)
    serializer = serializers.CoursesSerializer(courses, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes([JWTPermission])
def get_block(request, course_id, day_number):
    user = get_user_from_cookie(request)
    course_needed = get_object_or_404(Course, id=course_id)
    block_needed = get_object_or_404(Block, course=course_needed, day_number=day_number)
    block_serializer = serializers.BlockSerializer(block_needed)
    exercises = Exercise.objects.all().filter(block__exact=block_needed)
    exer_list = list(exercises)
    exer_list_ind = 0
    exercise_serializer = serializers.ExerciseSerializer(exercises, many=True)
    for exercise in exercise_serializer.data:
        tmp = get_object_or_404(Completed_Exercise, exercise=exer_list[exer_list_ind], user=user)
        compl_exer = serializers.CompletedExerciseSerializer(tmp)
        exercise['is_completed'] = compl_exer.data['is_completed']
        exer_list_ind += 1
    return Response({'block_data': block_serializer.data, 'exercises_data': exercise_serializer.data, 'days_cnt': Block.objects.all().filter(course__exact=course_needed).count()}, status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([JWTPermission])
def change_exercise_completion(request, exercise_id):
    user = get_user_from_cookie(request)
    exercise = get_object_or_404(Exercise, pk=exercise_id)
    compl_exer_obj = get_object_or_404(Completed_Exercise, exercise=exercise, user=user)
    if compl_exer_obj.is_completed:
        compl_exer_obj.time = None
    else:
        compl_exer_obj.time = timezone.now()
    compl_exer_obj.is_completed = not compl_exer_obj.is_completed
    try:
        compl_exer_obj.save()
    except:
        return Response('Error while saving', status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return Response('Exercise completion updated successfully!', status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([JWTPermission])
def create_payment(request, course_id):
    print("redirect_url" in request.data)

    user = get_user_from_cookie(request)
    course = get_object_or_404(Course, pk=course_id)
    print(course not in user.courses.all())
    if "redirect_url" in request.data and course not in user.courses.all():
        redirect_url = request.data["redirect_url"]
        print("here")
        old_pending_payments = Payment.objects.filter(
            course=course, user=user, status="pending"
        )
        if not len(old_pending_payments):
            yokassa_payment = get_payment_for_course(course, redirect_url)
            Payment.objects.create(
                course=course,
                user=user,
                yokassa_payment_id=yokassa_payment.id,
                url=yokassa_payment.confirmation.confirmation_url,
            )
            url = yokassa_payment.confirmation.confirmation_url
        else:
            url = old_pending_payments.first().url
        data = {"url": url}
        return Response(data, status=status.HTTP_200_OK)
    return Response("Bad request", status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
@permission_classes([JWTPermission])
def confirm_payment(request, course_id):
    user = get_user_from_cookie(request)
    course = get_object_or_404(Course, pk=course_id)
    payment = Payment.objects.filter(user=user, course=course).filter(
        ~Q(status="canceled")
    )
    if payment:
        payment = payment[0]
    else:
        return Response("Bad request", status=status.HTTP_400_BAD_REQUEST)
    if check_if_payment_succeed(payment.yokassa_payment_id):
        payment.status = "succeeded"
        for block_instance in course.blocks.all():
            for exercise_instance in block_instance.exercises.all():
                compl_exer = Completed_Exercise(exercise=exercise_instance, user=user, is_completed=False)
                compl_exer.save()
        user.courses.add(course)
        user.save()
        payment.save()
        return Response("OK", status=status.HTTP_200_OK)
    payment.status = "canceled"
    return Response("Bad request", status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes([JWTPermission])
def create_course(request):
    try:
        course_instance = Course(**request.data)
    except:
        return Response("Invalid data format!", status=status.HTTP_400_BAD_REQUEST)

    try:
        course_instance.save()
    except Exception as e:
        print(e)
        return Response(
            "Failed to save to database!", status=status.HTTP_500_INTERNAL_SERVER_ERROR
        )
    return Response("Course created successfully!", status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([JWTPermission])
def create_block(request):

    try:
        block_instance = Block()
        block_instance.name = request.data.get("name", "")
        block_instance.description = request.data.get("description", "")
        course_instance = Course.objects.get(pk=request.data["course_id"])
        block_instance.course = course_instance
        block_instance.save()
    except:
        return Response("Invalid data format!", status=status.HTTP_400_BAD_REQUEST)
    return Response("Block created successfully!", status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([JWTPermission])
def create_exercise(request):
    try:
        exercise_instance = Exercise()
        exercise_instance.name = request.data.get("name", "")
        exercise_instance.description = request.data.get("description", "")
        block_instance = Block.objects.get(pk=request.data["block_id"])
        exercise_instance.block = block_instance
        exercise_instance.save()
    except:
        return Response("Invalid data format!", status=status.HTTP_400_BAD_REQUEST)
    return Response("Exercise created successfully!", status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([AllowAny])
def save_image(request):
    c = Course.objects.get(pk=1)
    image = request.data["image"]
    c.image = image
    c.save()
    return Response("ok")
